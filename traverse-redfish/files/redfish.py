#!/usr/bin/env python3
import os
from bottle import route,run,template,request
import json
import time
import sys

RESET_GPIO = None
def toggle_gpio(gpio_num,value):
	sysfs_file_name = "/sys/class/gpio/gpio{}/value".format(gpio_num)
	sysfs_file = open(sysfs_file_name,'w')
	print(value,file=sysfs_file)
	sysfs_file.close()

@route('/redfish/v1/Systems/1/Actions/ComputerSystem.Reset/',method='POST')
def reset():
    global RESET_GPIO
    reqbody = request.body.read()
    print(reqbody)
    reqstring = reqbody.decode('us-ascii')
    redjson = json.loads(reqstring)
    print(redjson)
    if ("ResetType" in redjson):
        resettype = redjson["ResetType"]
        if (resettype == "ForceRestart"):
            toggle_gpio(RESET_GPIO,0)
            time.sleep(1)
            toggle_gpio(RESET_GPIO,1)

if (len(sys.argv) < 3):
    print("Syntax: {} port reset_gpio".format(sys.argv[0]))
    exit(1)

listen_port = sys.argv[1]
RESET_GPIO = sys.argv[2]
run(host='0.0.0.0',port=listen_port)
